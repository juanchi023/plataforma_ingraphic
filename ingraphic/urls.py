from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.home.urls')),
    path('', include('apps.blog.urls')),
    path('', include('apps.services.urls')),
]

handler404 = 'apps.home.views.error_404_view'

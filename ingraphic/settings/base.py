from pathlib import Path
from unipath import Path

BASE_DIR = Path(__file__).ancestor(3)


SECRET_KEY = '9c$hu%@41j8^0w_sb@$1f9w847k@ly0!35oqoo3gqjy8+qc4yj'

LANGUAGE_CODE = 'es-co'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',

    'import_export',
    'widget_tweaks',
    'ckeditor',
    # Local apps
    'apps.home',
    'apps.blog',
    'apps.services'
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'ingraphic.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR.child('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'ingraphic.estandar.backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
)

WSGI_APPLICATION = 'ingraphic.wsgi.application'


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# CELERY STUFF
CELERY_broker_url = 'redis://h:p5a1267ee476d6802892eaccdc92c303f0b8af8520109743f2add84d28d884e56@ec2-52-204-185-105.compute-1.amazonaws.com:11889'
result_backend = 'redis://h:p5a1267ee476d6802892eaccdc92c303f0b8af8520109743f2add84d28d884e56@ec2-52-204-185-105.compute-1.amazonaws.com:11889'
CELERY_BEAT_SCHEDULER = {}
accept_content = ['application/json']
task_serializer = 'json'
result_serializer = 'json'
timezone = 'America/Bogota'

STATIC_ROOT = BASE_DIR.child('staticfiles')

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    BASE_DIR.child('static')
]

# configuraciones de DROPBOX
DEFAULT_FILE_STORAGE = 'storages.backends.dropbox.DropBoxStorage'
DROPBOX_OAUTH2_TOKEN = 'o24jviFVvMEAAAAAAAAAAVJLl1zUnlVMfs6GsPM_I5IeRE6A6A54oRd8ws-yy6mh'

MEDIA_ROOT = '/ingraphic/'
MEDIA_URL = '/ingraphic/'

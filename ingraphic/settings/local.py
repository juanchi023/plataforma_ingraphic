from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_ingraphic',
        'USER': 'postgres',
        'PASSWORD': 'Cartagena023',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

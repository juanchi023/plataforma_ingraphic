from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_ingraphic_2021',
        'USER': 'us_1ngr4ph1c_2021',
        'PASSWORD': 'psw_Cartagena@192394.',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

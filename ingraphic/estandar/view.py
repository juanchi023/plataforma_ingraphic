from django.views.generic import View
from django.views.generic.edit import ModelFormMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class LoginMixin(View):

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(LoginMixin, self).dispatch(*args, **kwargs)

class InstanceMixin(ModelFormMixin,LoginMixin):

    def get_instance(self):
        return None

    def add_arguments(self):
        return None

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.
        """
        self.object = self.get_instance()
        kwargs = super(InstanceMixin, self).get_form_kwargs()
        arg = self.add_arguments()
        if arg:
            kwargs.update(self.add_arguments())
        return kwargs
from django.contrib import sitemaps
from django.urls import reverse
from apps.blog.models import Post
from apps.services.models import Service


class BlogSitemap(sitemaps.Sitemap):
    changefreq = "weekly"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return Post.objects.filter(published=True)

    def lastmod(self, obj):
        return obj.created_at


class ServiceSitemap(sitemaps.Sitemap):
    changefreq = "weekly"
    priority = 0.5
    protocol = 'https'

    def items(self):
        return Service.objects.filter(is_active=True)

    def lastmod(self, obj):
        return obj.created_at


class PageSitemap(sitemaps.Sitemap):
    priority = 1.0
    changefreq = 'weekly'
    protocol = 'https'

    def items(self):
        return [
            'home_view',
            'about_view',
            'contact_view',
            'podcast_view',
            'portfolio_view',
            'service_list_view',
        ]

    def location(self, item):
        return reverse(item)

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic import DetailView, ListView
from django.core import serializers
from celery import shared_task
from celery.decorators import task
from .models import *
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.views.decorators.http import require_POST
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from apps.blog.models import *
from apps.services.models import *
from .forms import *


class ComingSoonView(TemplateView):
    template_name = "landing/index.html"


class HomeView(TemplateView):
    template_name = "home/index.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['services'] = Service.objects.all().order_by('category__name')[:6]
        return context


class ServiceDetailView(DetailView):
    template_name = "home/service_detail.html"
    model = Service

    def get_queryset(self, **kwargs):
        return Service.objects.filter(slug=self.kwargs.get('slug'))


class AboutView(TemplateView):
    template_name = "home/about.html"


class ContactView(TemplateView):
    template_name = "home/contact.html"


class PodcastView(TemplateView):
    template_name = "home/podcast.html"


class PortfoliotView(TemplateView):
    template_name = "home/portfolio.html"


class ServicesListView(ListView):
    template_name = "home/services.html"
    model = Service
    context_object_name = 'services'
    ordering = 'created_at'


@task(name='send_email_nuevo_suscriptor')
def send_email_nuevo_suscriptor(email, name):
    print("{1} {0}".format(email, name))


@require_POST
def registrar_suscriptor(request):

    if request.method == 'POST':
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)

        suscriptor = Suscriptor()

        suscriptor.name = name
        suscriptor.email = email
        suscriptor.save()
        send_email_nuevo_suscriptor.delay(suscriptor.email, suscriptor.name)

        message = '¡Ya lo hemos guardado! Gracias por tu suscripción'
        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST
def registrar_mensaje(request):

    if request.method == 'POST':
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        subject = request.POST.get('subject', None)
        phone = request.POST.get('phone', None)
        message = request.POST.get('message', None)
        mensaje = Mensaje()

        mensaje.name = name
        mensaje.email = email
        mensaje.phone = phone
        mensaje.subject = subject
        mensaje.message = message
        mensaje.save()

        # send_email_nuevo_mensaje.delay(name, company, email, subject, phone)

        message = '¡Su solicitud se ha registrado! Nos comunicaremos contigo en breve.'
        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# Admins Panel
def admins_login_view(request):

    if not request.user.is_anonymous:
        return HttpResponseRedirect(reverse('admins_panel_view'))
    else:
        form = AdminEmailAuthForm(request.POST or None)

        if form.is_valid():
            login(request, form.get_user())
            return HttpResponseRedirect(reverse('admins_panel_view'))

    return render(request, 'panel/login.html', {'form': form})


@login_required
def admins_logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('admins_login_view'))


@login_required
def admins_panel_view(request):

    if request.user.is_staff:

        num_posts = Post.objects.all().count()

        return render(request, 'panel/admins/panel.html', {'num_posts': num_posts})
    else:
        return HttpResponseRedirect(reverse('home_view'))


def error_404_view(request, exception, template_name='home/404.html'):
    response = render(request, template_name)
    response.status_code = 404
    return response

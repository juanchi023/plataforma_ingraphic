from django.db import models
import os


class Mensaje(models.Model):

    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    subject = models.CharField(max_length=120)
    message = models.TextField(default="")
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0}: {1} [{2}]".format(self.name, self.email, self.fecha)


class Suscriptor(models.Model):

    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0}: {1} [{2}]".format(self.name, self.email, self.date)

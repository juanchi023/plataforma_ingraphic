from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate
from .models import *


class AdminEmailAuthForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True)

    def __init__(self, *args, **kwargs):
        self.user_cache = None
        super(AdminEmailAuthForm, self).__init__(*args, **kwargs)

    def clean(self):

        self.cleaned_data = super(AdminEmailAuthForm, self).clean()        
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        self.user_cache = authenticate(email=email, password=password)
        if self.user_cache is None:
            self._errors["email"] = self.error_class(['Email o contraseña incorrecta!'])
        elif not self.user_cache.is_active:
            self._errors["email"] = self.error_class(['¡Usuario inactivo o email no confirmado!'])        

        return self.cleaned_data

    def get_user(self):
        return self.user_cache

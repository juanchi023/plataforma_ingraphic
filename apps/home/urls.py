from django.contrib import admin
from django.urls import path, include
from apps.home import views as home_views
from django.views.generic.base import TemplateView
from django.contrib.sitemaps.views import sitemap
from apps.home.sitemaps import *


sitemaps = {
    'static_pages': PageSitemap,
    'posts': BlogSitemap,
    'services': ServiceSitemap,
}

urlpatterns = [
    path('', home_views.HomeView.as_view(), name='home_view'),
    path('quienes-somos/', home_views.AboutView.as_view(), name='about_view'),
    path('contacto/', home_views.ContactView.as_view(), name='contact_view'),
    path('podcast/', home_views.PodcastView.as_view(), name='podcast_view'),
    path('portafolio/', home_views.PortfoliotView.as_view(), name='portfolio_view'),
    path('servicios/', home_views.ServicesListView.as_view(), name='service_list_view'),
    path('servicios/<slug>/', home_views.ServiceDetailView.as_view(), name='service_detail_view'),

    path('comingsoon/', home_views.ComingSoonView.as_view(), name='comingsoonview'),
    path('registrar-suscriptor/', home_views.registrar_suscriptor, name='registrar_suscriptor'),
    path('registrar-mensaje/', home_views.registrar_mensaje, name='registrar_mensaje'),
    path("robots.txt/", TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),
    path('sitemap.xml/', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),

    # Admins Panel
    path('admins/login/', home_views.admins_login_view, name='admins_login_view'),
    path('admins/logout/', home_views.admins_logout_view, name='admins_logout_view'),
    path('admins/panel/', home_views.admins_panel_view, name='admins_panel_view'),
]

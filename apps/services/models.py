import os
from django.contrib.auth import get_user_model
from django.db import models
from ingraphic.estandar.models import BaseModel
from django.template.defaultfilters import slugify
from django.urls import reverse
from ckeditor.fields import RichTextField


class ServiceCategory(BaseModel):

    name = models.CharField(max_length=40)
    icon = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    class Meta:
        verbose_name = 'Categoría de servicios'
        verbose_name_plural = 'Categorías de servicios'

    def __str__(self):
        return "{0}".format(self.name)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(ServiceCategory, self).save(*args, **kwargs)


def upload_service_image(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'servicios/{0}/{1}{2}'.format(instance.id, filename_base, filename_ext.lower())


class Service(BaseModel):

    title = models.CharField(max_length=60)
    image = models.ImageField(upload_to=upload_service_image, blank=True)
    category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    views = models.IntegerField(default=0)
    short_description = models.CharField(max_length=200)
    description = RichTextField(blank=True, null=True)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    class Meta:
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'

    def __str__(self):
        return "{0}".format(self.title)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Service, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('service_detail_view', kwargs={'slug': self.slug})

from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin


@admin.register(Post, Category, Comment)
class BlogAdmin(ImportExportModelAdmin):
    pass

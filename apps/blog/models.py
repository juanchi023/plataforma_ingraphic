import os
from django.contrib.auth import get_user_model
from django.db import models
from ingraphic.estandar.models import BaseModel
from django.template.defaultfilters import slugify
from django.urls import reverse


class Category(BaseModel):

    name = models.CharField(max_length=40)
    icon = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    class Meta:
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'

    def __str__(self):
        return "{0}".format(self.name)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


def upload_imagen_post(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'blog/post/{0}/{1}{2}'.format(instance.id, filename_base, filename_ext.lower())


class Post(BaseModel):

    image = models.ImageField(upload_to=upload_imagen_post, blank=True)
    title = models.CharField(max_length=400, editable=True)
    categories = models.ManyToManyField(Category, blank=True)
    slug = models.SlugField(editable=True, unique=True, null=True, blank=True, max_length=400)
    view = models.BigIntegerField(default=0, null=True, blank=True)
    short_description = models.CharField(max_length=200)
    published = models.BooleanField(default=False)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Publicación'
        verbose_name_plural = 'Publicaciones'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return "{0}".format(self.title)

    def get_absolute_url(self):
        return reverse('single_post', kwargs={'slug': self.slug})


class Comment(BaseModel):

    user = models.CharField(max_length=500, blank=False)
    email = models.CharField(max_length=500, blank=False)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'

    def __str__(self):
        return "{0} - {1} - {2}".format(self.post.title, self.user, self.email)

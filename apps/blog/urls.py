from django.urls import path
from . import views


urlpatterns = [
    path('blog/', views.home_blog, name='home_blog'),
    path('blog/busqueda/', views.busqueda_palabra_clave, name='busqueda_palabra_clave'),
    path('blog/categoria/<slug>/',views.filtro_category, name='filtro_category'),
    path('blog/post/<slug:slug>/',views.single_post, name='single_post'),
    path('blog/post-comment/',views.post_comment, name='post_comment'),


    #============ Administradores ============

    #Publicaciones
    path('panel/admins/blog/gestion-posts/', views.gestion_post_admins
    	, name='gestion_post_admins'),

    path('panel/admins/blog/gestion-post/publicar-post/', views.cambiar_estado_publicacion
        , name='cambiar_estado_publicacion'),

    path('panel/admins/blog/gestion-post/borrar-post/', views.borrar_publicacion
        , name='borrar_publicacion'),

    path('panel/admins/blog/gestion-post/nuevo-post/', views.nueva_publicacion_admin
        , name='nueva_publicacion_admin'),

    path('panel/admins/blog/gestion-post/editar-post/<slug:slug>'
        , views.editar_publicacion_admin
        , name='editar_publicacion_admin'),

    #Categorías
    path('panel/admins/blog/gestion-categorias/', views.gestion_categoria_admins
        , name='gestion_categoria_admins'),

    path('panel/admins/blog/gestion-post/nueva-categoria/', views.nueva_categoria_admin
        , name='nueva_categoria_admin'),

    path('panel/admins/blog/gestion-post/editar-categoria/<slug:slug>/'
        , views.editar_categoria_admin
        , name='editar_categoria_admin'),

    path('panel/admins/blog/gestion-post/borrar-categoria/', views.borrar_categoria
        , name='borrar_categoria'),

    

    ]

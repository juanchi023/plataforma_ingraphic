from django import template
from apps.blog.models import *

register = template.Library()

@register.filter
def get_count_posts(categoria):

	num = Post.objects.filter(categoria=categoria).count()

	return num

@register.filter
def get_count_comment(comment):

    num = comment.count()

    return num
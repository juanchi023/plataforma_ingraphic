from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.views.generic import ListView, CreateView, DetailView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.db.models import Q


def home_blog(request):

    posts = Post.objects.all().order_by('-created_at')
    categories = Category.objects.all()
    paginator = Paginator(posts, 10)

    page = request.GET.get('page')
    posts = paginator.get_page(page)

    return render(request, 'home/blog.html', {'posts': posts, 'categories': categories})


def single_post(request, *args, **kwargs):
    list_posts = Post.objects.all().order_by('created_at')[:3]
    post = Post.objects.get(slug=kwargs.get('slug'))

    categories = Category.objects.all()
    comments = Comment.objects.filter(post__slug=kwargs.get('slug')).order_by('-created_at')
    return render(request, 'home/blog_detail.html', {
        'post': post,
        'categories': categories,
        'list_posts': list_posts,
        'comments': comments,
    })


def busqueda_palabra_clave(request):

    categories = Category.objects.all()

    busqueda = request.GET.get('busqueda')

    list_posts = Post.objects.all().order_by('-created_at')[:5]

    posts = Post.objects.filter(
        Q(titulo__icontains=busqueda) | Q(categoria__nombre__icontains=busqueda)).distinct().order_by('-created_at')

    paginator = Paginator(posts, 10)
    try:
        page = request.GET['page']
    except Exception:
        page = 1
    posts = paginator.get_page(page)

    return render(request, 'home/blog/resultados_palabra_clave.html', {
        'posts': posts,
        'busqueda': busqueda,
        'categories': categories,
        'posts': posts,
        'list_posts': list_posts
    })


def filtro_category(request, *args, **kwargs):

    category = Category.objects.get(slug=kwargs.get('slug'))

    categories = Category.objects.all()

    posts = Post.objects.filter(categories__id=category.id).order_by('-created_at')

    list_posts = Post.objects.all().order_by('created_at')[:3]

    paginator = Paginator(posts, 10)
    try:
        page = request.GET['page']
    except Exception:
        page = 1
    posts = paginator.get_page(page)

    return render(request, 'home/filter_category.html', {
        'posts': posts,
        'category': category,
        'list_posts': list_posts,
        'categories': categories
    })


@require_POST
def post_comment(request, *args, **kwargs):
    if request.method == 'POST':
        email = request.POST.get('email', None)
        user = request.POST.get('user', None)
        mensaje = request.POST.get('comment', None)
        slug = request.POST.get('slug', None)

        comment = Comment()
        comment.user = user
        comment.email = email
        comment.description = mensaje
        comment.post = Post.objects.get(slug=slug)
        comment.save()

        message = 'Su comentario se ha enviado!'

        ctx = {'message': message}

        return HttpResponse(json.dumps(ctx), content_type='application/json')


def error_404(request):
    return render(request, 'home/blog/404.html')

# ================== Administradores ==================


# ______________ Categorías ______________
@login_required
def gestion_categoria_admins(request):

    categories = Category.objects.all()
    return render(request, 'panel/admins/blog/gestion-categorias.html', {'categories': categories})


@login_required
def nueva_categoria_admin(request):
    form = NuevaCategoriaForm(request.POST or None)

    if form.is_valid():
        form.save()
        url = reverse('gestion_categoria_admins')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admins/blog/nueva-categoria.html', {'form': form})


@login_required
def editar_categoria_admin(request, *args, **kwargs):

    categoria = Category.objects.get(slug=kwargs.get('slug'))

    form = EditCategoriaForm(request.POST or None, instance=categoria)

    if form.is_valid():
        form.save()
        url = reverse('gestion_categoria_admins')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admins/blog/editar-categoria.html', {'form': form})


@login_required
@require_POST
def borrar_categoria(request):
    message = ""

    if request.method == 'POST':

        id_cat = request.POST.get('id_cat', None)
        category = Category.objects.get(id=id_cat)

        category.delete()

        message = '¡La categoría fue eliminada exitosamente!'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# ______________ posts ______________
@login_required
def gestion_post_admins(request):

    posts = Post.objects.all().order_by('-created_at')
    return render(request, 'panel/admins/blog/gestion-post.html', {'posts': posts})


@login_required
def nueva_publicacion_admin(request):

    form = NuevoPostForm(request.POST or None, request.FILES or None)
    categories = Category.objects.all()

    if form.is_valid():
        post = form.save(commit=False)
        post.user = request.user
        post.save()
        cat2 = request.POST.getlist('categories2')
        for cat in cat2:
            post.categories.add(Category.objects.get(id=cat))
        url = reverse('gestion_post_admins')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admins/blog/nuevo-post.html', {'form': form, 'categories': categories})


@login_required
def editar_publicacion_admin(request, *args, **kwargs):

    post = Post.objects.get(slug=kwargs.get('slug'))

    form = EditPostForm(request.POST or None, request.FILES or None, instance=post)

    categories = Category.objects.all()

    if form.is_valid():
        post = form.save(commit=False)
        cat2 = request.POST.getlist('categories2')
        post.categories.clear()
        for cat in cat2:
            post.categories.add(Category.objects.get(id=cat))
        post.save()
        url = reverse('gestion_post_admins')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admins/blog/editar-post.html', {'form': form, 'categories': categories})


@login_required
@require_POST
def borrar_publicacion(request):
    message = ""

    if request.method == 'POST':

        id_post = request.POST.get('id_post', None)
        publicacion = Post.objects.get(id=id_post)

        publicacion.delete()

        message = '¡La publicación fue eliminada exitosamente!'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@require_POST
def cambiar_estado_publicacion(request):
    message = ""

    if request.method == 'POST':

        id_post = request.POST.get('id_post', None)
        publicacion = Post.objects.get(id=id_post)

        if publicacion.publicado:
            publicacion.publicado = False
            publicacion.save()
            message = 'La publicacion se ocultó exitosamente'
        elif not publicacion.publicado:
            publicacion.publicado = True
            publicacion.save()
            message = 'La publicacion fue publicada exitosamente'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

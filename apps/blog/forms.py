from __future__ import unicode_literals
from django.forms import ModelForm
from django import forms
from .models import *


class NuevoPostForm(forms.ModelForm):

    class Meta:
        model = Post
        exclude = (
            'created_at',
            'modified',
            'user',
            'slug',
            'views',
            'categories',
        )

    def __init__(self, *args, **kwargs):
        super(NuevoPostForm, self).__init__(*args, **kwargs)


class EditPostForm(forms.ModelForm):

    class Meta:
        model = Post
        exclude = (
            'created_at',
            'modified',
            'user',
            'slug',
            'views',
            'categories',
        )

    def __init__(self, *args, **kwargs):
        super(EditPostForm, self).__init__(*args, **kwargs)


class NuevaCategoriaForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'icon')


class EditCategoriaForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'slug', 'icon')
